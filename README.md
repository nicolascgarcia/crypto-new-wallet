<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo_text.svg" width="320" alt="Nest Logo" /></a>
</p>

[circleci-image]: https://img.shields.io/circleci/build/github/nestjs/nest/master?token=abc123def456
[circleci-url]: https://circleci.com/gh/nestjs/nest

## Description

This is a GraphQL API for crypto wallet application.

## Installation

```bash
$ yarn install
```

## Environment Variables

Create a `.env` file with this content:

```bash
DATABASE_URI='mongo connecting'
TATUM_API_KEY='tatum api key'
TATUM_API_URL=https://api-us-west1.tatum.io
WEBHOOK_API=https://api-url/webhook

JWT_SECRET="JWT secret"
JWT_ACCESS_TOKEN_EXPIRATION_TIME=30
JWT_REFRESH_TOKEN_SECRET="JWT refresh token secret"

```

## Running the app

```bash
# development
$ yarn start

# watch mode
$ yarn start:dev

# production mode
$ yarn start:prod
```

## Deploying

```bash
# build
$ yarn build

# production mode
$ yarn start:prod
```

## Test

```bash
# unit tests
$ yarn test

```

P.S.: Unfortunately I could not develop all needed tests due to an error with Tatum API dependecies.
