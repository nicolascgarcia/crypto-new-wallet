import { Resolver, Mutation, Args } from '@nestjs/graphql';
import { AuthService } from './auth.service';
import { Auth } from './entities/auth.entity';
import { AuthenticateInput } from './dto/authenticate.input';
import { Public } from '@/common/decorators/public.decorator';

@Resolver(() => Auth)
export class AuthResolver {
  constructor(private readonly authService: AuthService) {}

  @Public()
  @Mutation(() => Auth)
  login(@Args('authenticateInput') authenticateInput: AuthenticateInput) {
    return this.authService.authenticate(authenticateInput);
  }
}
