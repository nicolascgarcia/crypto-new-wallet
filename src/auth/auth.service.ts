import { HttpStatus, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { handlePasswordEncryption } from '@/common/encrypt/handlePasswordEncryption';
import { CustomException } from '@/common/exceptions/custom.exception';
import { UsersService } from '@/users/users.service';
import { AuthenticateInput } from './dto/authenticate.input';

@Injectable()
export class AuthService {
  constructor(
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService,
  ) {}

  async authenticate(authenticateInput: AuthenticateInput) {
    const { email, password } = authenticateInput;

    const { masterPasswordHash, stretchedMasterKey } = handlePasswordEncryption(
      email,
      password,
    );

    const user = await this.usersService.findOneByEmail(email);

    if (!user) {
      throw new CustomException(
        {
          message: 'There is no user with this email',
        },
        HttpStatus.UNAUTHORIZED,
      );
    }

    const comparePassword =
      masterPasswordHash.toString('base64') === user.password;

    if (!comparePassword) {
      throw new CustomException(
        {
          message: 'Your password is incorrect',
        },
        HttpStatus.UNAUTHORIZED,
      );
    }

    const { _id } = user;

    const payload = {
      _id,
      stretchedMasterKey: stretchedMasterKey.macKey.toString('base64'),
    };

    return {
      token: this.jwtService.sign(payload),
    };
  }
}
