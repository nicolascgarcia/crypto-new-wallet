
/*
 * -------------------------------------------------------
 * THIS FILE WAS AUTOMATICALLY GENERATED (DO NOT MODIFY)
 * -------------------------------------------------------
 */

/* tslint:disable */
/* eslint-disable */
export interface AuthenticateInput {
    email: string;
    password: string;
}

export interface CreateUserInput {
    email: string;
    fullName: string;
    password: string;
}

export interface TransactionInput {
    amount: string;
    currency: string;
    mnemonic: string;
    recipientAddress: string;
    senderAccountId: string;
}

export interface Account {
    accountCode?: Nullable<string>;
    active: boolean;
    balance: AccountBalance;
    created: string;
    currency: string;
    customerId?: Nullable<string>;
    frozen: boolean;
    id: string;
    xpub?: Nullable<string>;
}

export interface AccountBalance {
    accountBalance: string;
    availableBalance: string;
}

export interface AccountIncomming {
    amount: string;
    blockHash: string;
    blockHeight: number;
    currency: string;
    date: number;
    from: string;
    id: string;
    index: number;
    reference: string;
    to: string;
    txId: string;
}

export interface Auth {
    token: string;
}

export interface Encrypted {
    data: string;
    iv: string;
}

export interface MarketValue {
    amount?: Nullable<string>;
    currency?: Nullable<string>;
    source?: Nullable<string>;
    sourceDate?: Nullable<number>;
}

export interface IMutation {
    createUser: User;
    login: Auth;
    transaction: string;
}

export interface IQuery {
    allTransactions: Transaction[];
    getAccount: Account;
    getExchange: Rate;
    transactionsByCurrency: Transaction[];
    wallets: WalletDecrypted[];
}

export interface Rate {
    basePair: string;
    id: string;
    source: string;
    timestamp: number;
    value: string;
}

export interface ISubscription {
    addedTransaction: AccountIncomming;
}

export interface Transaction {
    accountId: string;
    address?: Nullable<string>;
    amount: string;
    anonymous: boolean;
    attr?: Nullable<string>;
    counterAccountId?: Nullable<string>;
    created: number;
    currency: string;
    marketValue: MarketValue;
    operationType: string;
    paymentId?: Nullable<string>;
    recipientNote?: Nullable<string>;
    reference: string;
    senderNote?: Nullable<string>;
    transactionCode?: Nullable<string>;
    transactionType: string;
    txId?: Nullable<string>;
}

export interface User {
    _id: string;
    createdAt: number;
    customerId: Encrypted;
    email: string;
    fullName: string;
    password: string;
    symmetricKey: Encrypted;
    updatedAt: number;
}

export interface WalletDecrypted {
    _id: string;
    accountId: string;
    createdAt: number;
    currency: string;
    depositAddress: string;
    mnemonic: string;
    updatedAt: number;
    userId: string;
}

type Nullable<T> = T | null;
