import { Field, ObjectType } from '@nestjs/graphql';
import { Prop } from '@nestjs/mongoose';

@ObjectType()
export class Encrypted {
  @Prop()
  @Field(() => String, { description: 'Encrypted data initial vector' })
  iv: string;

  @Prop({ unique: true })
  @Field(() => String, { description: 'Encrypted data' })
  data: string;
}
