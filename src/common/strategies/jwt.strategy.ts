import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { HttpStatus, Injectable } from '@nestjs/common';
import { UsersService } from '@/users/users.service';
import { decryptDecipheriv } from '../encrypt/cipheriv';
import { CustomException } from '../exceptions/custom.exception';

export type PayloadType = {
  _id: string;
  stretchedMasterKey: string;
  symmetricKey: string;
  customerId: string;
};

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly usersService: UsersService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: process.env.JWT_SECRET,
    });
  }

  async validate(payload: PayloadType) {
    const userId: string = payload._id;

    const user = await this.usersService.findOne(userId);

    if (!user) {
      throw new CustomException(
        {
          message: 'Unauthorized',
        },
        HttpStatus.UNAUTHORIZED,
      );
    }

    const stretchedMasterKeyBuffer = Buffer.from(
      payload.stretchedMasterKey,
      'base64',
    );

    const symmetricKey = decryptDecipheriv(
      user.symmetricKey.data,
      stretchedMasterKeyBuffer,
      user.symmetricKey.iv,
    );

    const customerId = decryptDecipheriv(
      user.customerId.data,
      symmetricKey,
      user.customerId.iv,
    ).toString();

    return {
      ...payload,
      symmetricKey: symmetricKey.toString('base64'),
      customerId,
    };
  }
}
