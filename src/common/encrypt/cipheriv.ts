import {
  BinaryLike,
  CipherKey,
  createCipheriv,
  createDecipheriv,
  randomBytes,
} from 'crypto';

export const encryptCipheriv = (key: CipherKey, data: BinaryLike) => {
  const initVector = randomBytes(16);

  const cipher = createCipheriv('aes-256-cbc', key, initVector);

  return {
    iv: initVector.toString('base64'),
    data: Buffer.concat([cipher.update(data), cipher.final()]).toString(
      'base64',
    ),
  };
};

export const decryptDecipheriv = (
  encrypedData: string,
  key: CipherKey,
  iv: string,
) => {
  const ivBuffer = Buffer.from(iv, 'base64');
  const dataBuffer = Buffer.from(encrypedData, 'base64');

  const decipher = createDecipheriv('aes-256-cbc', key, ivBuffer);

  const decripted = Buffer.concat([
    decipher.update(dataBuffer),
    decipher.final(),
  ]);

  return decripted;
};
