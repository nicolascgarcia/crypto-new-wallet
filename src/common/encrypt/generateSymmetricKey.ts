import { randomBytes } from 'crypto';

export const generateSymmetricKey = () => {
  const key = randomBytes(64);
  const encKey = Buffer.from(key.slice(0, key.length / 2));
  const macKey = Buffer.from(key.slice(key.length / 2));

  return {
    key,
    encKey,
    macKey,
  };
};
