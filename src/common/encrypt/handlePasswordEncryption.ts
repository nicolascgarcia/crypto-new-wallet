import { hkdfSync, pbkdf2Sync } from 'crypto';

export const handlePasswordEncryption = (email: string, password: string) => {
  const masterKey = pbkdf2Sync(password, email, 100000, 64, 'sha256');

  const masterPasswordHash = pbkdf2Sync(masterKey, password, 1, 64, 'sha256');

  const stretchedMasterKey = (() => {
    const hash = Buffer.from(hkdfSync('sha512', masterKey, '', '', 64));
    const encKey = Buffer.from(hash.slice(0, hash.length / 2));
    const macKey = Buffer.from(hash.slice(hash.length / 2));
    return {
      hash,
      encKey,
      macKey,
    };
  })();

  return {
    masterKey,
    masterPasswordHash,
    stretchedMasterKey,
  };
};
