import { Test, TestingModule } from '@nestjs/testing';
import { TatumService } from './tatum.service';
import { Currency, Fiat } from '@tatumio/tatum';
import { Transaction } from './entities/transaction.entity';
import { Rate } from './entities/rate.entity';
import { Account } from './entities/account.entity';
import { TransactionInput } from './dto/transaction.input';
import * as faker from 'faker';

jest.mock('@tatumio/tatum', () => ({
  Currency: ['ETH', 'MATIC', 'CELO'],
  generateWallet: jest.fn().mockImplementation(() => ({
    xpub: faker.datatype.uuid(),
    mnemonic: faker.lorem.sentence(),
  })),
  Fiat: ['EUR'],
  createAccount: jest.fn().mockImplementation(() => ({
    id: faker.datatype.uuid(),
    customerId: faker.datatype.uuid(),
  })),
  generateDepositAddress: jest.fn().mockImplementation(() => ({
    address: faker.finance.bitcoinAddress(),
  })),
  SubscriptionType: ['ACCOUNT_INCOMING_BLOCKCHAIN_TRANSACTION'],
  createNewSubscription: jest.fn(),
  getTransactionsByCustomer: jest.fn().mockReturnValue(Array<Transaction>()),
  getTransactionsByAccount: jest.fn().mockReturnValue(Array<Transaction>()),
  getExchangeRate: jest.fn().mockImplementation(
    (currency: string): Rate => ({
      id: currency as Currency | Fiat,
      value: faker.commerce.price(),
      basePair: 'EUR' as Fiat,
      timestamp: Date.now(),
      source: faker.internet.url(),
    }),
  ),
  getAccountById: jest.fn().mockImplementation(
    (id: string): Account => ({
      id,
      balance: {
        accountBalance: '0',
        availableBalance: '0',
      },
      active: true,
      created: Date.now().toString(),
      currency: 'EUR',
      frozen: false,
      customerId: faker.datatype.uuid(),
      xpub: faker.datatype.uuid(),
    }),
  ),
  getDepositAddressesForAccount: jest
    .fn()
    .mockImplementation(() => [{ derivationKey: 0 }]),
  generatePrivateKeyFromMnemonic: jest
    .fn()
    .mockImplementation(() => faker.datatype.uuid()),
  sendEthOffchainTransaction: jest
    .fn()
    .mockImplementation(() => ({ txId: faker.datatype.uuid() })),
  sendCeloOffchainTransaction: jest
    .fn()
    .mockImplementation(() => ({ txId: faker.datatype.uuid() })),
  sendPolygonOffchainTransaction: jest
    .fn()
    .mockImplementation(() => ({ txId: faker.datatype.uuid() })),
}));

describe('TatumService', () => {
  let service: TatumService;

  afterEach(() => {
    jest.clearAllMocks();
  });

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TatumService],
    }).compile();

    service = module.get<TatumService>(TatumService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('createFullAcount', () => {
    it('Should create a complete tatum account', async () => {
      const currency = faker.random.arrayElement([
        Currency.ETH,
        Currency.MATIC,
        Currency.CELO,
      ]);
      const userId = faker.datatype.uuid();

      expect(await service.createFullAcount(currency, userId)).toEqual({
        accountId: expect.any(String),
        customerId: expect.any(String),
        depositAddress: expect.any(String),
        wallet: expect.any(String),
      });
    });
  });

  describe('getAllTransactions', () => {
    it('Should return all users wallets transactions', async () => {
      const customerId = faker.datatype.uuid();

      expect(await service.getAllTransactions(customerId)).toEqual(
        Array<Transaction>(),
      );
    });
  });

  describe('getTransactionsByCurrency', () => {
    it('Should return all transactions by account/currency', async () => {
      const accountId = faker.datatype.uuid();

      expect(await service.getTransactionsByCurrency(accountId)).toEqual(
        Array<Transaction>(),
      );
    });
  });

  describe('getExchange', () => {
    it('Should return currenct exchange rate for a currency', async () => {
      const currency = faker.random.arrayElement([
        Currency.ETH,
        Currency.MATIC,
        Currency.CELO,
      ]);

      expect(await service.getExchange(currency)).toEqual({
        id: currency,
        value: expect.any(String),
        basePair: expect.any(String),
        timestamp: expect.any(Number),
        source: expect.any(String),
      });
    });
  });

  describe('getAccount', () => {
    it('Should return a account by its id', async () => {
      const accountId = faker.datatype.uuid();

      expect(await service.getAccount(accountId)).toEqual({
        id: accountId,
        balance: {
          accountBalance: expect.any(String),
          availableBalance: expect.any(String),
        },
        active: expect.any(Boolean),
        created: expect.any(String),
        currency: expect.any(String),
        frozen: expect.any(Boolean),
        customerId: expect.any(String),
        xpub: expect.any(String),
      });
    });
  });

  describe('makeTransaction', () => {
    it('Should make a transaction', async () => {
      const currency = faker.random.arrayElement([
        Currency.ETH,
        Currency.MATIC,
        Currency.CELO,
      ]);

      const payload: TransactionInput = {
        recipientAddress: faker.finance.bitcoinAddress(),
        senderAccountId: faker.datatype.uuid(),
        amount: faker.commerce.price(),
        mnemonic: faker.lorem.sentence(),
        currency,
      };

      expect(await service.makeTransaction(payload)).toEqual(
        expect.any(String),
      );
    });
  });
});
