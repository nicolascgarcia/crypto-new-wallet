import { Field, Float, Int, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class AccountIncomming {
  @Field(() => Float, { nullable: true })
  date?: number;

  @Field(() => String, { nullable: true })
  amount?: string;

  @Field(() => String, { nullable: true })
  currency?: string;

  @Field(() => String, { nullable: true })
  id?: string;

  @Field(() => String, { nullable: true })
  reference?: string;

  @Field(() => String, { nullable: true })
  txId?: string;

  @Field(() => String, { nullable: true })
  blockHash?: string;

  @Field(() => Float, { nullable: true })
  blockHeight?: number;

  @Field(() => String, { nullable: true })
  from?: string;

  @Field(() => String, { nullable: true })
  to?: string;

  @Field(() => Int, { nullable: true })
  index?: number;
}
