import { InputType, Field } from '@nestjs/graphql';
import { Currency } from '@tatumio/tatum';

@InputType()
export class TransactionInput {
  @Field(() => String)
  recipientAddress: string;

  @Field(() => String)
  senderAccountId: string;

  @Field(() => String)
  amount: string;

  @Field(() => String)
  mnemonic: string;

  @Field(() => String)
  currency: Currency;
}
