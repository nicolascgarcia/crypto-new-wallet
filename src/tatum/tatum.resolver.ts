import { Args, Mutation, Query, Resolver, Subscription } from '@nestjs/graphql';
import { TatumService } from './tatum.service';
import { GqlUserDecorator } from '@/common/decorators/gql-jwt-user.decorator';
import { Transaction } from './entities/transaction.entity';
import { Currency, Fiat } from '@tatumio/tatum';
import { Rate } from './entities/rate.entity';
import { Account } from './entities/account.entity';
import { TransactionInput } from './dto/transaction.input';
import { Public } from '@/common/decorators/public.decorator';
import { PubSubEngine } from 'graphql-subscriptions';
import { Inject } from '@nestjs/common';
import { AccountIncomming } from './dto/account-incomming.input';
import { PayloadType } from '@/common/strategies/jwt.strategy';

@Resolver()
export class TatumResolver {
  constructor(
    private readonly tatumService: TatumService,
    @Inject('PUB_SUB')
    private pubSub: PubSubEngine,
  ) {}

  @Public()
  @Subscription(() => AccountIncomming, {
    filter: (payload, variables) => payload.userId === variables.userId,
  })
  addedTransaction(@Args('userId') userId: string) {
    return this.pubSub.asyncIterator('addedTransaction');
  }

  @Query(() => [Transaction])
  allTransactions(@GqlUserDecorator() { customerId }: PayloadType) {
    return this.tatumService.getAllTransactions(customerId);
  }

  @Query(() => [Transaction])
  transactionsByCurrency(@Args('accountId') accountId: string) {
    return this.tatumService.getTransactionsByCurrency(accountId);
  }

  @Query(() => Rate)
  getExchange(@Args('currency') currency: Currency | Fiat) {
    return this.tatumService.getExchange(currency);
  }

  @Query(() => Account)
  getAccount(@Args('accountId') accountId: string) {
    return this.tatumService.getAccount(accountId);
  }

  @Mutation(() => String)
  transaction(@Args('TransactionInput') transactionInput: TransactionInput) {
    return this.tatumService.makeTransaction(transactionInput);
  }
}
