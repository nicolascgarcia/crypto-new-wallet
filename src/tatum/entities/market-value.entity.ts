import { Field, Float, ObjectType } from '@nestjs/graphql';
import { Fiat } from '@tatumio/tatum';

@ObjectType()
export class MarketValue {
  @Field(() => String, { nullable: true })
  amount?: string;

  @Field(() => String, { nullable: true })
  currency?: Fiat;

  @Field(() => Float, { nullable: true })
  sourceDate?: number;

  @Field(() => String, { nullable: true })
  source?: string;
}
