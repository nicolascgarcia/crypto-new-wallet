import { Field, Float, ObjectType } from '@nestjs/graphql';
import { Currency, Fiat } from '@tatumio/tatum';

@ObjectType()
export class Rate {
  @Field(() => String)
  id: Fiat | Currency;

  @Field(() => String)
  value: string;

  @Field(() => String)
  basePair: Fiat;

  @Field(() => Float)
  timestamp: number;

  @Field(() => String)
  source: string;
}
