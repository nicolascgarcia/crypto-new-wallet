import { Field, ObjectType } from '@nestjs/graphql';
import { AccountBalance } from './account-balance.entity';

@ObjectType()
export class Account {
  @Field(() => String, { nullable: true })
  accountCode?: string;

  @Field(() => String)
  id: string;

  @Field(() => AccountBalance)
  balance: AccountBalance;

  @Field(() => String)
  created: string;

  @Field(() => String)
  currency: string;

  @Field(() => String, { nullable: true })
  customerId?: string;

  @Field(() => Boolean)
  frozen: boolean;

  @Field(() => Boolean)
  active: boolean;

  @Field(() => String, { nullable: true })
  xpub?: string;
}
