import { Field, Float, ObjectType } from '@nestjs/graphql';
import { OperationType, TransactionType } from '@tatumio/tatum';
import { MarketValue } from './market-value.entity';

@ObjectType()
export class Transaction {
  @Field(() => String)
  accountId: string;

  @Field(() => String)
  amount: string;

  @Field(() => Boolean)
  anonymous: boolean;

  @Field(() => String, { nullable: true })
  counterAccountId?: string;

  @Field(() => String)
  currency: string;

  @Field(() => Float)
  created: number;

  @Field(() => MarketValue)
  marketValue: MarketValue;

  @Field(() => String)
  operationType: OperationType;

  @Field(() => String, { nullable: true })
  paymentId?: string;

  @Field(() => String, { nullable: true })
  attr?: string;

  @Field(() => String, { nullable: true })
  address?: string;

  @Field(() => String, { nullable: true })
  recipientNote?: string;

  @Field(() => String)
  reference: string;

  @Field(() => String, { nullable: true })
  txId?: string;

  @Field(() => String, { nullable: true })
  senderNote?: string;

  @Field(() => String, { nullable: true })
  transactionCode?: string;

  @Field(() => String)
  transactionType: TransactionType;
}
