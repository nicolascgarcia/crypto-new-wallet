import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class AccountBalance {
  @Field(() => String)
  accountBalance: string;

  @Field(() => String)
  availableBalance: string;
}
