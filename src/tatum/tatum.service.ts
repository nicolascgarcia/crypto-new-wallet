import { Injectable } from '@nestjs/common';
import {
  createAccount,
  CreateAccount,
  createNewSubscription,
  Currency,
  Fiat,
  generateDepositAddress,
  generatePrivateKeyFromMnemonic,
  generateWallet,
  getAccountById,
  getDepositAddressesForAccount,
  getExchangeRate,
  getTransactionsByAccount,
  getTransactionsByCustomer,
  sendCeloOffchainTransaction,
  sendEthOffchainTransaction,
  sendPolygonOffchainTransaction,
  SubscriptionType,
} from '@tatumio/tatum';
import { TransactionInput } from './dto/transaction.input';

@Injectable()
export class TatumService {
  private async generateBlockchainWallet(currency: Currency) {
    return await generateWallet(currency, true);
  }

  private async createNewAccount(account: CreateAccount) {
    return await createAccount(account);
  }

  private async createNewDepositAddress(id: string) {
    return await generateDepositAddress(id);
  }

  private async createSubscription(
    type: SubscriptionType,
    virtualAccountId: string,
    userId: string,
  ) {
    return await createNewSubscription({
      type,
      attr: {
        id: virtualAccountId,
        url: `${process.env.WEBHOOK_API}/${userId}`,
      },
    });
  }

  async getAllTransactions(customerId: string) {
    return await getTransactionsByCustomer({ id: customerId });
  }

  async getTransactionsByCurrency(accountId: string) {
    return await getTransactionsByAccount({
      id: accountId,
    });
  }

  async getExchange(currency: Currency | Fiat) {
    return await getExchangeRate(currency);
  }

  async getAccount(accountId: string) {
    return await getAccountById(accountId);
  }

  async createFullAcount(currency: Currency, userId: string) {
    const wallet = (await this.generateBlockchainWallet(
      currency,
    )) as unknown as { xpub: string; mnemonic: string };

    const virtualAccount = await this.createNewAccount({
      currency,
      xpub: (wallet as unknown as { xpub: string }).xpub,
      accountingCurrency: Fiat.EUR,
      customer: {
        externalId: userId,
      },
    });

    const depositAddress = await this.createNewDepositAddress(
      virtualAccount.id,
    );

    await this.createSubscription(
      SubscriptionType.ACCOUNT_INCOMING_BLOCKCHAIN_TRANSACTION,
      virtualAccount.id,
      userId,
    );

    return {
      customerId: virtualAccount.customerId,
      wallet: wallet.mnemonic,
      accountId: virtualAccount.id,
      depositAddress: depositAddress.address,
    };
  }

  async makeTransaction({
    recipientAddress,
    senderAccountId,
    amount,
    mnemonic,
    currency,
  }: TransactionInput) {
    const senderAccount = await getDepositAddressesForAccount(senderAccountId);
    const privateKey = await generatePrivateKeyFromMnemonic(
      currency,
      true,
      mnemonic,
      senderAccount[0].derivationKey,
    );

    switch (currency) {
      case Currency.ETH: {
        const transaction = await sendEthOffchainTransaction(true, {
          senderAccountId,
          address: recipientAddress,
          amount,
          privateKey,
        });
        return (transaction as unknown as { txId: string }).txId;
      }
      case Currency.CELO: {
        const transaction = await sendCeloOffchainTransaction(true, {
          senderAccountId,
          address: recipientAddress,
          amount,
          feeCurrency: Currency.CELO,
          privateKey,
        });
        return (transaction as unknown as { txId: string }).txId;
      }
      case Currency.MATIC: {
        const transaction = await sendPolygonOffchainTransaction(true, {
          address: recipientAddress,
          amount,
          senderAccountId,
          privateKey,
        });
        return (transaction as unknown as { txId: string }).txId;
      }
    }
  }
}
