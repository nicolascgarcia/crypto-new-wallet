import { Module } from '@nestjs/common';
import { TatumService } from './tatum.service';
import { TatumResolver } from './tatum.resolver';
import { WebhookModule } from '@/webhook/webhook.module';

@Module({
  imports: [WebhookModule],
  providers: [TatumResolver, TatumService],
  exports: [TatumService],
})
export class TatumModule {}
