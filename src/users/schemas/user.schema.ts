import { Field, Float, ObjectType } from '@nestjs/graphql';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { Encrypted } from '@/common/encryption/entities/encrypted.entity';

export type UserDocument = User & Document;

@Schema()
@ObjectType()
export class User {
  @Field(() => String, { description: "User's id" })
  _id: string;

  @Prop()
  @Field(() => String, { description: "User's full name" })
  fullName: string;

  @Prop({ unique: true })
  @Field(() => String, { description: "User's email" })
  email: string;

  @Prop()
  @Field(() => String, { description: "User's password" })
  password: string;

  @Prop()
  @Field(() => Encrypted, { description: "User's symmetric key" })
  symmetricKey: Encrypted;

  @Prop()
  @Field(() => Encrypted, { description: "User's customer id" })
  customerId: Encrypted;

  @Prop({ default: new Date() })
  @Field(() => Float, { description: "User's create date" })
  createdAt: number;

  @Prop({ default: new Date() })
  @Field(() => Float, { description: "User's last update date" })
  updatedAt: number;
}

export const UserSchema = SchemaFactory.createForClass(User);
