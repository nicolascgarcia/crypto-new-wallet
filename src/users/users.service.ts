import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { CreateUserInput } from './dto/create-user.input';
import { User, UserDocument } from './schemas/user.schema';
import { Model } from 'mongoose';
import { TatumService } from '@/tatum/tatum.service';
import { Currency } from '@tatumio/tatum';
import { WalletsService } from '@/wallets/wallets.service';
import { handlePasswordEncryption } from '@/common/encrypt/handlePasswordEncryption';
import { encryptCipheriv } from '@/common/encrypt/cipheriv';
import { generateSymmetricKey } from '@/common/encrypt/generateSymmetricKey';

@Injectable()
export class UsersService {
  constructor(
    @InjectModel(User.name)
    private userModel: Model<UserDocument>,
    private readonly tatumService: TatumService,
    private readonly walletsService: WalletsService,
  ) {}

  async create(createUserInput: CreateUserInput) {
    const symmetricKey = generateSymmetricKey();

    const { stretchedMasterKey, masterPasswordHash } = handlePasswordEncryption(
      createUserInput.email,
      createUserInput.password,
    );

    const encryptedSymmetricKey = encryptCipheriv(
      stretchedMasterKey.macKey,
      symmetricKey.encKey,
    );

    const createdUser = await this.userModel.create({
      ...createUserInput,
      password: masterPasswordHash.toString('base64'),
      symmetricKey: encryptedSymmetricKey,
    });

    // Generating ETH
    const eth = await this.tatumService.createFullAcount(
      Currency.ETH,
      createdUser.id,
    );

    const encryptedEthAccount = encryptCipheriv(
      symmetricKey.encKey,
      eth.accountId,
    );

    const encryptedEthMnemonic = encryptCipheriv(
      symmetricKey.encKey,
      eth.wallet,
    );

    await this.walletsService.create({
      userId: createdUser._id,
      accountId: encryptedEthAccount,
      depositAddress: eth.depositAddress,
      mnemonic: encryptedEthMnemonic,
      currency: Currency.ETH,
    });

    // Generating CELO
    const celo = await this.tatumService.createFullAcount(
      Currency.CELO,
      createdUser.id,
    );

    const encryptedCeloAccount = encryptCipheriv(
      symmetricKey.encKey,
      celo.accountId,
    );

    const encryptedCeloMnemonic = encryptCipheriv(
      symmetricKey.encKey,
      celo.wallet,
    );

    await this.walletsService.create({
      userId: createdUser._id,
      accountId: encryptedCeloAccount,
      depositAddress: celo.depositAddress,
      mnemonic: encryptedCeloMnemonic,
      currency: Currency.CELO,
    });

    // Generating MATIC
    const matic = await this.tatumService.createFullAcount(
      Currency.MATIC,
      createdUser.id,
    );

    const encryptedMaticAccount = encryptCipheriv(
      symmetricKey.encKey,
      matic.accountId,
    );

    const encryptedMaticMnemonic = encryptCipheriv(
      symmetricKey.encKey,
      matic.wallet,
    );

    await this.walletsService.create({
      userId: createdUser._id,
      accountId: encryptedMaticAccount,
      depositAddress: matic.depositAddress,
      mnemonic: encryptedMaticMnemonic,
      currency: Currency.MATIC,
    });

    // customerId
    const encryptedCustomerIdAccount = encryptCipheriv(
      symmetricKey.encKey,
      matic.customerId,
    );

    return await this.userModel.findByIdAndUpdate(
      createdUser._id,
      {
        customerId: encryptedCustomerIdAccount,
      },
      { new: true },
    );
  }

  async findOneByEmail(email: string) {
    return await this.userModel.findOne({ email }).exec();
  }

  async findOne(userId: string) {
    return await this.userModel.findById(userId);
  }
}
