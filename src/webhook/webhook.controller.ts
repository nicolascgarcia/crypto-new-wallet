import { AccountIncomming } from '@/tatum/dto/account-incomming.input';
import { Controller, Post, Body, Param, Inject } from '@nestjs/common';
import { PubSubEngine } from 'graphql-subscriptions';
import { WebhookService } from './webhook.service';
import { Public } from '@/common/decorators/public.decorator';

@Controller('webhook')
export class WebhookController {
  constructor(
    private readonly webhookService: WebhookService,
    @Inject('PUB_SUB')
    private pubSub: PubSubEngine,
  ) {}

  @Public()
  @Post(':id')
  create(
    @Param() params: { id: string },
    @Body() createWebhookDto: AccountIncomming,
  ) {
    this.pubSub.publish('addedTransaction', {
      addedTransaction: createWebhookDto,
      userId: params.id,
    });
    return this.webhookService.create(createWebhookDto, params.id);
  }
}
