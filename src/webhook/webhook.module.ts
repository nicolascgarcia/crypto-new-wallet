import { Module } from '@nestjs/common';
import { WebhookService } from './webhook.service';
import { WebhookController } from './webhook.controller';
import { PubSub } from 'graphql-subscriptions';

@Module({
  controllers: [WebhookController],
  providers: [
    WebhookService,
    {
      provide: 'PUB_SUB',
      useValue: new PubSub(),
    },
  ],
  exports: [
    {
      provide: 'PUB_SUB',
      useExisting: 'PUB_SUB',
    },
  ],
})
export class WebhookModule {}
