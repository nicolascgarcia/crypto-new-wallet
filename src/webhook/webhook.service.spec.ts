import { AccountIncomming } from '@/tatum/dto/account-incomming.input';
import { Test, TestingModule } from '@nestjs/testing';
import { WebhookService } from './webhook.service';
import * as faker from 'faker';

describe('WebhookService', () => {
  let service: WebhookService;

  const pubSubMock = {
    subscribe: jest.fn(),
  };

  afterEach(() => {
    jest.clearAllMocks();
  });

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [WebhookService, { provide: 'PUB_SUB', useValue: pubSubMock }],
    }).compile();

    service = module.get<WebhookService>(WebhookService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('create', () => {
    it('Should send incomming transaction data to subscription', () => {
      const dto: AccountIncomming = {
        amount: faker.commerce.price(),
        blockHash: faker.datatype.uuid(),
        blockHeight: faker.datatype.number(2),
        currency: 'ETH',
        date: Date.now(),
        from: faker.datatype.uuid(),
        id: faker.datatype.uuid(),
        index: faker.datatype.number(2),
        reference: faker.datatype.uuid(),
        to: faker.datatype.uuid(),
        txId: faker.lorem.sentence(),
      };
      const userId = faker.datatype.uuid();

      service.create(dto, userId);
      expect(pubSubMock.subscribe).toBeCalled();
    });
  });
});
