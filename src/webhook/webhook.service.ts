import { AccountIncomming } from '@/tatum/dto/account-incomming.input';
import { Inject, Injectable } from '@nestjs/common';
import { PubSub } from 'graphql-subscriptions';

@Injectable()
export class WebhookService {
  constructor(@Inject('PUB_SUB') private pubSub: PubSub) {}

  create(createWebhookDto: AccountIncomming, userId: string) {
    const addedTransaction = () => createWebhookDto;
    this.pubSub.subscribe('addedTransaction', addedTransaction);
    return {
      createWebhookDto,
      userId,
    };
  }
}
