import { Field, Float, ObjectType } from '@nestjs/graphql';
import { Prop } from '@nestjs/mongoose';
import { Currency } from '@tatumio/tatum';

@ObjectType()
export class WalletDecrypted {
  @Field(() => String, { description: "Wallet's id" })
  _id: string;

  @Prop()
  @Field(() => String, { description: "Wallet's user id" })
  userId: string;

  @Prop()
  @Field(() => String, { description: "Wallet's tatum account id" })
  accountId: string;

  @Prop()
  @Field(() => String, { description: "Wallet's deposit address" })
  depositAddress: string;

  @Prop()
  @Field(() => String, { description: "Wallet's mnemonic" })
  mnemonic: string;

  @Prop()
  @Field(() => String, { description: "Wallet's currency" })
  currency: Currency;

  @Prop({ default: new Date() })
  @Field(() => Float, { description: "Wallet's create date" })
  createdAt: number;

  @Prop({ default: new Date() })
  @Field(() => Float, { description: "Wallet's last update date" })
  updatedAt: number;
}
