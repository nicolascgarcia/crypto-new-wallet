import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { CreateWalletInput } from './dto/create-wallet.input';
import { Model } from 'mongoose';
import { Wallet, WalletDocument } from './schemas/wallet.schema';

@Injectable()
export class WalletsService {
  constructor(
    @InjectModel(Wallet.name)
    private walletModel: Model<WalletDocument>,
  ) {}

  async create(createWalletInput: CreateWalletInput) {
    return await this.walletModel.create(createWalletInput);
  }

  async findOne(userId: string) {
    return await this.walletModel.findOne({ userId });
  }

  async getUserWallets(userId: string) {
    return await this.walletModel.find({ userId });
  }
}
