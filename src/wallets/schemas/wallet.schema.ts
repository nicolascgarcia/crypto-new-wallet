import { Field, Float, ObjectType } from '@nestjs/graphql';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Currency } from '@tatumio/tatum';
import { Document } from 'mongoose';
import { Encrypted } from '../../common/encryption/entities/encrypted.entity';

export type WalletDocument = Wallet & Document;

@Schema()
@ObjectType()
export class Wallet {
  @Field(() => String, { description: "Wallet's id" })
  _id: string;

  @Prop()
  @Field(() => String, { description: "Wallet's user id" })
  userId: string;

  @Prop()
  @Field(() => Encrypted, { description: "Wallet's tatum account id" })
  accountId: Encrypted;

  @Prop()
  @Field(() => String, { description: "Wallet's deposit address" })
  depositAddress: string;

  @Prop()
  @Field(() => Encrypted, { description: "Wallet's mnemonic" })
  mnemonic: Encrypted;

  @Prop()
  @Field(() => String, { description: "Wallet's currency" })
  currency: Currency;

  @Prop({ default: new Date() })
  @Field(() => Float, { description: "Wallet's create date" })
  createdAt: number;

  @Prop({ default: new Date() })
  @Field(() => Float, { description: "Wallet's last update date" })
  updatedAt: number;
}

export const WalletSchema = SchemaFactory.createForClass(Wallet);
