import { InputType, Field } from '@nestjs/graphql';
import { Currency } from '@tatumio/tatum';
import { Encrypted } from '@/common/encryption/entities/encrypted.entity';

@InputType()
export class CreateWalletInput {
  @Field(() => String, { description: "Wallet's user id" })
  userId: string;

  @Field(() => Encrypted, {
    description: "Wallet's tatum account id",
  })
  accountId: Encrypted;

  @Field(() => String, { description: "Wallet's deposit address" })
  depositAddress: string;

  @Field(() => Encrypted, {
    description: "Wallet's mnemonic",
  })
  mnemonic: Encrypted;

  @Field(() => Currency, { description: "Wallet's currency" })
  currency: Currency;
}
