import { PayloadType } from '@/common/strategies/jwt.strategy';
import { Parent, Query, ResolveField, Resolver } from '@nestjs/graphql';
import { GqlUserDecorator } from '@/common/decorators/gql-jwt-user.decorator';
import { decryptDecipheriv } from '@/common/encrypt/cipheriv';
import { WalletDecrypted } from './entities/wallet-decripted.entity';
import { Wallet } from './schemas/wallet.schema';
import { WalletsService } from './wallets.service';

@Resolver(() => WalletDecrypted)
export class WalletsResolver {
  constructor(private readonly walletsService: WalletsService) {}

  @Query(() => [WalletDecrypted])
  wallets(
    @GqlUserDecorator()
    { _id: userId }: PayloadType,
  ) {
    return this.walletsService.getUserWallets(userId);
  }

  @ResolveField()
  mnemonic(
    @Parent() { mnemonic }: Wallet,
    @GqlUserDecorator()
    { symmetricKey }: PayloadType,
  ) {
    const symmetricKeyBuffer = Buffer.from(symmetricKey, 'base64');

    return decryptDecipheriv(
      mnemonic.data,
      symmetricKeyBuffer,
      mnemonic.iv,
    ).toString();
  }

  @ResolveField()
  accountId(
    @Parent() { accountId }: Wallet,
    @GqlUserDecorator()
    { symmetricKey }: PayloadType,
  ) {
    const symmetricKeyBuffer = Buffer.from(symmetricKey, 'base64');

    return decryptDecipheriv(
      accountId.data,
      symmetricKeyBuffer,
      accountId.iv,
    ).toString();
  }
}
