import { getModelToken } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { CreateWalletInput } from './dto/create-wallet.input';
import { Wallet } from './schemas/wallet.schema';
import { WalletsService } from './wallets.service';
import * as faker from 'faker';
import { Currency } from '@tatumio/tatum';

jest.mock('../common/encrypt/cipheriv');

describe('WalletsService', () => {
  let service: WalletsService;

  afterEach(() => {
    jest.clearAllMocks();
  });

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        WalletsService,
        {
          provide: getModelToken(Wallet.name),
          useValue: {
            create: jest
              .fn()
              .mockImplementation((input: CreateWalletInput) => ({
                _id: faker.datatype.uuid(),
                ...input,
              })),
            findOne: jest.fn().mockImplementation((search: Wallet) => search),
            find: jest.fn().mockImplementation((search: Wallet) => [search]),
          },
        },
      ],
    }).compile();

    service = module.get<WalletsService>(WalletsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('create', () => {
    it('Should create a new wallet', async () => {
      const input: CreateWalletInput = {
        accountId: {
          data: faker.datatype.uuid(),
          iv: faker.datatype.uuid(),
        },
        userId: faker.datatype.uuid(),
        currency: 'ETH' as Currency,
        depositAddress: faker.finance.bitcoinAddress(),
        mnemonic: {
          data: faker.datatype.uuid(),
          iv: faker.datatype.uuid(),
        },
      };

      expect(await service.create(input)).toEqual({
        _id: expect.any(String),
        ...input,
      });
    });
  });

  describe('findOne', () => {
    it('Should return a wallet by its user id', async () => {
      const userId = faker.datatype.uuid();

      expect(await service.findOne(userId)).toEqual({ userId });
    });
  });

  describe('getUserWallets', () => {
    it('Should return all wallet by theirs user id', async () => {
      const userId = faker.datatype.uuid();

      expect(await service.getUserWallets(userId)).toEqual([{ userId }]);
    });
  });
});
